<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'smartapple' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '-t[UA8q=o!9i~_0-twDBd8cUgjfRY5IX?w,sTye{8Q$8d@l@U@yQfaX6^oESuK8E' );
define( 'SECURE_AUTH_KEY',  'o3<zt%+1ig7SR+3Fii*D]f8}n@Q+8=[C.I8tcfIID(=7~qYzmF8Xj]a@[,*z t&5' );
define( 'LOGGED_IN_KEY',    '?2N5J}}Z5PB1*VWXT|m7_o#t@bz-vBaeH>$?)B&Wwqm=XvvD-{!8T|MAM@ZkHrp,' );
define( 'NONCE_KEY',        'HN=D&25G*g.EItWMq:fM`M*$Hm`C9Bo;T|U;b %QGs]hS8v8,~ivJ0o;~=2#7Q)W' );
define( 'AUTH_SALT',        'lIKwlS%zmjYZ]@Le7o(|@|``izc%_A*}8&jmy!!5>,/-#qM<4Q:{Ye|XLI6{lx-z' );
define( 'SECURE_AUTH_SALT', 'NMn}251Y(C44_/b}K]V1!+!)xav!8G=s>p;`Hs4H[}G%g!i1>p&|`&|~#w$mhVY7' );
define( 'LOGGED_IN_SALT',   'rgO.e<Qu]hdJJ:F%$AY}AY&EEgQ`35nd;fg)-pBJZf@R2r!vE(vFz,qS#;*gjxBI' );
define( 'NONCE_SALT',       'HsE#>R`T2@t)jmyl)d.cRBi:ytsL nN *]HfV_ML=o+ wHUfll>G[`Nm^Wa6quB%' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
