<?php

/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since Twenty Seventeen 1.0
 * @version 1.0
 */

get_header(); ?>

<section id="content" class="content-block ">
	<div class="container error404">
		<h1><span class="pink">4</span><span class="blue">0</span><span class="green">4</span></h1>
		<h2>Sorry, that page doesn’t exist</h2>
		<p>
			The link you followed may be broken, or the page may have been removed.<br>
			Use the navigation above or go to the <a href="https://www.wareable.com">Wareable homepage</a>.
		</p>
	</div>
</section>
<?php
get_footer();
