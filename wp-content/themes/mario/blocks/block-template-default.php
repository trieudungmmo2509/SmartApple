<?php
$template = block_value("template");
$args = [
    "title" =>  block_value("title"),
    "number_post" => block_value("number-posts"),
    "category" => block_value("choose-category")
];
?>
<?php
if ($template == TEMPLATE_HOME_TOP) {
    wpse_get_template_part('template-parts/template_home_top', null, $args);
} else if ($template == TEMPLATE_LASTEST_STORIES) {
    wpse_get_template_part('template-parts/template_latest_stories', null, $args);
} else if ($template == TEMPLATE_CATEGORIES_DEFAULT) {
    wpse_get_template_part('template-parts/template_categories_default', null, $args);
} else if ($template == TEMPLATE_HEALTH) {
    wpse_get_template_part('template-parts/template_health', null, $args);
} else if ($template == TEMPLATE_POPULAR_STORIES) {
    wpse_get_template_part('template-parts/template_popular_stories', null, $args);
} else {
    wpse_get_template_part('template-parts/template_smart_watches', null, $args);
}
?>