<?php
get_header();
$category = get_queried_object();
$paged = 1;    //hoặc 0
if (get_query_var('paged')) {
    $paged = get_query_var('paged');
} elseif (get_query_var('page')) {
    $paged = get_query_var('page');
}
$posts_per_page = 42;  //posts per page
$post_category = query_posts_category_pagination($category->term_id, $posts_per_page, $paged);
if ($post_category->have_posts()) :
?>
    <section id="content" class="content-block ">
        <div class="container-fluid container-fluid--max pt-7 pb-5">
            <header class="row">
                <div class="offset-lg-2 col-lg-8">
                    <div class="accent-line-after">
                        <h2 class="text-center font-8 font-title pb-5">
                            <?php echo $category->name; ?>
                        </h2>
                    </div>
                    <p class="text-center font-4 font-teaser pt-5 m-0"> <?php echo $category->category_description; ?>
                    </p>
                </div>
            </header>
        </div>
        <div class="pb-6 pb-sm-10">
            <div class="container-fluid container-fluid--max">
                <div class="row">
                    <?php
                    $i = 0;
                    while ($post_category->have_posts()) : $post_category->the_post();
                        $i++;
                    ?>
                        <?php
                        if ($i < 21 || ($i > 26 && $i < 35)) :
                        ?>
                            <div class=" col-md-6 col-lg-3 card-container cb-editable">
                                <div class="cb-editable"><a href="<?php echo get_permalink(); ?>">
                                        <div class="up-card up-card--white up-card--sm-horizontal-small up-card--md-horizontal-small-title up-card--lg-vertical up-card--xl-vertical">
                                            <img class="lazyload up-card__image up-card__image--small" sizes="
        (min-width: 1260px) 150px,
        (min-width: 992px) 150px,
        (min-width: 768px) 625px,
        150px" src="<?php echo get_the_post_thumbnail_url(get_the_ID()) ?>" alt="<?php the_title(); ?>" />
                                            <div class="up-card__text-container">
                                                <div class="up-card__text">
                                                    <h3 class="up-card__title"><?php the_title(); ?>
                                                    </h3>
                                                    <div class="up-card__teaser"><?php echo get_post_meta(get_the_ID(), "description", true); ?></div>
                                                </div>
                                            </div>
                                            <div class="up-card__image-overlay up-card__image up-card__image--small"></div>
                                        </div>
                                    </a></div>
                            </div>
                        <?php else : ?>
                            <div class=" col-md-6 card-container cb-editable">
                                <div class="cb-editable"><a href="<?php echo get_permalink(); ?>">
                                        <div class="up-card up-card--white up-card--sm-horizontal up-card--md-horizontal-small-title up-card--lg-horizontal up-card--xl-horizontal">
                                            <img class="lazyload up-card__image up-card__image--small" sizes="
        (min-width: 1260px) 150px,
        (min-width: 992px) 150px,
        (min-width: 768px) 625px,
        150px" src="<?php echo get_the_post_thumbnail_url(get_the_ID()) ?>" alt="<?php the_title(); ?>" />
                                            <div class="up-card__text-container">
                                                <div class="up-card__text">
                                                    <h3 class="up-card__title"><?php the_title(); ?></h3>
                                                    <div class="up-card__teaser"><?php echo get_post_meta(get_the_ID(), "description", true); ?></div>
                                                </div>
                                            </div>
                                            <div class="up-card__image-overlay up-card__image up-card__image--small"></div>
                                        </div>
                                    </a></div>
                            </div>
                    <?php
                        endif;
                    endwhile;
                    ?>

                </div>
                <?php if (function_exists("pagination")) {
                    pagination($post_category->max_num_pages);
                } ?>
            </div>

        </div>
    </section>
<?php
endif;
get_footer();
?>