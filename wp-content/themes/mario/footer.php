<footer class="footer py-8">
	<section class="footer__container container">
		<!-- <div class="footer-menu row">
			<div class="col-12 col-lg-auto footer-menu row">
				<div class="footer-menu__item mb-5 col-12 col-md-auto pl-md-0 pr-md-7"><a class="footer-menu__link" href="contact.html">Contact Us</a></div>
				<div class="footer-menu__item mb-5 col-12 col-md-auto pl-md-0 pr-md-7"><a class="footer-menu__link" href="about.html">About</a></div>
				<div class="footer-menu__item mb-5 col-12 col-md-auto pl-md-0 pr-md-7"><a class="footer-menu__link" href="team.html">The Team</a></div>
				<div class="footer-menu__item mb-5 col-12 col-md-auto pl-md-0 pr-lg-7"><a class="footer-menu__link" href="promise.html">Editorial Promise</a></div>
			</div>
			<div class="col-12 col-lg-auto footer-menu row">
				<div class="footer-menu__item mb-5 col-12 col-md-auto pl-md-0 pr-md-7"><a class="footer-menu__link" href="privacy-policy.html">Privacy Policy</a></div>
				<div class="footer-menu__item mb-5 col-12 col-md-auto pl-md-0 pr-md-7"><a class="footer-menu__link" href="assets/dist/static/downloads/Wareable%20Media%20information%202018%20v3.pdf">Advertise
						with us</a></div>
				<div class="footer-menu__item mb-5 col-12 col-md-auto pl-md-0"><a class="footer-menu__link" href="licensing.html">Licensing</a></div>
			</div>
		</div> -->
		<div class="footer-copyright row">
			<div class="col-md-12">
				Copyright Hanshi Ltd. All rights reserved.
			</div>
		</div>
	</section>
</footer>
</div>

<?php wp_footer(); ?>
</body>

</html>