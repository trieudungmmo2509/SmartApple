<?php

/**
 * Twenty Seventeen functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 */
define('TEMPLATE_HOME_TOP', 'template_home_top');
define('TEMPLATE_LASTEST_STORIES', 'template_latest_stories');
define('TEMPLATE_CATEGORIES_DEFAULT', 'template_categories_default');
define('TEMPLATE_HEALTH', 'template_health');
define('TEMPLATE_POPULAR_STORIES', 'template_popular_stories');
define('TEMPLATE_SMART_WATCHES', 'template_smart_watches');
define('TEMPLATE_CATEGORY', 'template_category ');
/**
 * Twenty Seventeen only works in WordPress 4.7 or later.
 */
if (version_compare($GLOBALS['wp_version'], '4.7-alpha', '<')) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}
function register_menu_option()
{
	register_nav_menu('primary', 'Primary Menu');
	register_nav_menu('secondary', 'Secondary Menu');
}

add_action('after_setup_theme', 'register_menu_option');
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function mario_setup()
{
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/twentyseventeen
	 * If you're building a theme based on Twenty Seventeen, use a find and replace
	 * to change 'twentyseventeen' to the name of your theme in all the template files.
	 */
	load_theme_textdomain('mario');

	// Add default posts and comments RSS feed links to head.
	add_theme_support('automatic-feed-links');

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support('title-tag');

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support('post-thumbnails');

	add_image_size('mario-featured-image', 2000, 1200, true);

	add_image_size('mario-thumbnail-avatar', 100, 100, true);

	// Set the default content width.
	$GLOBALS['content_width'] = 525;
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support(
		'html5',
		array(
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		)
	);

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support(
		'post-formats',
		array(
			'aside',
			'image',
			'video',
			'quote',
			'link',
			'gallery',
			'audio',
		)
	);

	// Add theme support for Custom Logo.
	add_theme_support(
		'custom-logo',
		array(
			'width'      => 250,
			'height'     => 250,
			'flex-width' => true,
		)
	);

	// Add theme support for selective refresh for widgets.
	add_theme_support('customize-selective-refresh-widgets');

	// Load regular editor styles into the new block-based editor.
	add_theme_support('editor-styles');

	// Load default block styles.
	add_theme_support('wp-block-styles');

	// Add support for responsive embeds.
	add_theme_support('responsive-embeds');
}
add_action('after_setup_theme', 'mario_setup');
function query_posts_category_pagination($categoryId = 0, $posts_per_page = 42, $paged)
{
	$args = array(
		'post_type' => 'post',
		'post_status' => array('publish'),
		'cat'    => $categoryId,
		'numberposts' => -1,
		'posts_per_page' => $posts_per_page,
		'paged' => $paged
	);
	return new WP_Query($args);
}
function pagination_post_category()
{
	global $wp_query;

	$total_pages = $wp_query->max_num_pages;

	if ($total_pages > 1) {
		$current_page = max(1, get_query_var('paged'));

		echo paginate_links(array(
			'base' => get_pagenum_link(1) . '%_%',
			'format' => 'page/%#%',
			'current' => $current_page,
			'total' => $total_pages,
			'end_size'     => 5,
			'mid_size'     => 5,
			'prev_next'    => true,
			'prev_text'    => sprintf('<i></i> %1$s', __('Previous', 'text-domain')),
			'next_text'    => sprintf('%1$s <i></i>', __('Next', 'text-domain')),
		));
	}
}
function pagination($pages = '', $range = 4)
{
	$showitems = ($range * 2) + 1;
	global $paged;
	if (empty($paged)) $paged = 1;

	if ($pages == '') {
		global $wp_query;
		$pages = $wp_query->max_num_pages;
		if (!$pages) {
			$pages = 1;
		}
	}

	if (1 != $pages) {
		echo "<div class=\"pagination-block\"><span>Page " . $paged . " of " . $pages . "</span>";
		if ($paged > 2 && $paged > $range + 1 && $showitems < $pages) echo "<a href='" . get_pagenum_link(1) . "'>&laquo; First</a>";
		if ($paged > 1 && $showitems < $pages) echo "<a href='" . get_pagenum_link($paged - 1) . "'>&lsaquo; Previous</a>";

		for ($i = 1; $i <= $pages; $i++) {
			if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems)) {
				echo ($paged == $i) ? "<span class=\"current\">" . $i . "</span>" : "<a href='" . get_pagenum_link($i) . "' class=\"inactive\">" . $i . "</a>";
			}
		}

		if ($paged < $pages && $showitems < $pages) echo "<a href=\"" . get_pagenum_link($paged + 1) . "\">Next &rsaquo;</a>";
		if ($paged < $pages - 1 &&  $paged + $range - 1 < $pages && $showitems < $pages) echo "<a href='" . get_pagenum_link($pages) . "'>Last &raquo;</a>";
		echo "</div>\n";
	}
}
function query_post_current($postID = 0, $arrCategoryId = [], $posts_per_page = 6)
{
	$args = array(
		'post_type' => 'post',
		'post_status' => array('publish'),
		'posts_per_page' => $posts_per_page,
		'category__in'    => $arrCategoryId,
		'orderby' => "count",
		'order'   => "DESC",
		'post__not_in' => array($postID)
	);
	return new WP_Query($args);
}

function query_posts_with_by_category($categoryId = 0, $posts_per_page = 6)
{
	$args = array(
		'post_type' => 'post',
		'post_status' => array('publish'),
		'posts_per_page' => $posts_per_page,
		'cat'    => $categoryId,
		'meta_key'		=> 'is_show_home_page',
		'meta_value'	=> 1
	);
	return new WP_Query($args);
}


function wpse_get_template_part($slug, $name = null, $data = [])
{
	// here we're copying more of what get_template_part is doing.
	$templates = [];
	$name = (string) $name;

	if ('' !== $name) {
		$templates[] = "{$slug}-{$name}.php";
	}

	$templates[] = "{$slug}.php";

	$template = locate_template($templates, false);

	if (!$template) {
		return;
	}

	if ($data) {
		extract($data);
	}

	include($template);
}

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function mario_widgets_init()
{
	register_sidebar(
		array(
			'name'          => __('Blog Sidebar', 'mario'),
			'id'            => 'sidebar-1',
			'description'   => __('Add widgets here to appear in your sidebar on blog posts and archive pages.', 'twentyseventeen'),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

	register_sidebar(
		array(
			'name'          => __('Footer 1', 'mario'),
			'id'            => 'sidebar-2',
			'description'   => __('Add widgets here to appear in your footer.', 'twentyseventeen'),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

	register_sidebar(
		array(
			'name'          => __('Footer 2', 'mario'),
			'id'            => 'sidebar-3',
			'description'   => __('Add widgets here to appear in your footer.', 'twentyseventeen'),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action('widgets_init', 'mario_widgets_init');

/**
 * Register and Enqueue Styles.
 */
/** * Enqueues scripts and styles. */
function mariosmart_scripts()
{
	$theme_version = wp_get_theme()->get('Version');
	wp_enqueue_style('main-css', get_template_directory_uri() . '/style.css', null, $theme_version, false);
	if (is_front_page()) {
		wp_enqueue_style('index-css', get_template_directory_uri() . '/assets/css/index.css', null, $theme_version, false);
	} else if (is_single()) {
		wp_enqueue_style('article-css', get_template_directory_uri() . '/assets/css/article.css', null,  $theme_version, false);
	} else if (is_archive()) {
		wp_enqueue_style('category-css', get_template_directory_uri() . '/assets/css/category.css', null, $theme_version, false);
	} else if (is_search()) {
		wp_enqueue_style('search-css', get_template_directory_uri() . '/assets/css/search.css', null, $theme_version, false);
	} else {
		wp_enqueue_style('topics-css', get_template_directory_uri() . '/assets/css/topics.css', null, $theme_version, false);
	}
	wp_enqueue_script('index-script', get_template_directory_uri() . '/assets/js/index.js', array(), $theme_version, true);
	wp_enqueue_script('index-inline', get_template_directory_uri() . '/assets/js/index-inline.js', array(), $theme_version, true);
}
add_action('wp_enqueue_scripts', 'mariosmart_scripts');


function smartwatch_register_widget()
{

	register_widget('smartwatch_widget');
}
add_action('widgets_init', 'smartwatch_register_widget');
class smartwatch_widget extends WP_Widget
{
	function __construct()
	{

		parent::__construct(

			// widget ID

			'smartwatch_widget',

			__('Post of category', ' smartwatch_widget_domain'),

			array('description' => __('show post of category', 'smartwatch_widget_domain'),)

		);
	}

	public function widget($args, $instance)
	{

		$title = (!empty($instance['title'])) ? $instance['title'] : __('Post category');
		$number = (!empty($instance['number'])) ? absint($instance['number']) : 5;
		if (!$number) {
			$number = 5;
		}
		$categoryname = isset($instance['categoryname']) ? $instance['categoryname'] : false;
		$the_query = new WP_Query(array(
			'category_name' => $instance['categoryname'],
			'posts_per_page' => 5,
			'post_status'      => 'publish',

		));
		if (!$the_query->have_posts()) {
			return;
		}
?>
		<?php echo $args['before_widget']; ?>
		<div class="up-widget mb-6">
			<div class="fonts-sidebar-title position-relative">
				<?php
				if ($title) {
					echo  $title;
				}
				?>
			</div>
			<div class="d-flex flex-wrap recent-reviews clearfix">

				<?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

					<div class="col-sm-15 col-xl-15 col-lg-15 col-md-7 recent-review text-center fonts-recent-reviews ">
						<a href="<?php the_permalink(); ?>" class="cb-editable">
							<div class="review-text text-left">
								<div class="text-left"></div>
								<?php the_title(); ?>
							</div><img loading="lazy" src="<?php the_post_thumbnail('thumbnail'); ?>" alt="Amazfit T-Rex Pro" />
						</a>
					</div>
				<?php endwhile; ?>
			</div>
		</div>
	<?php
		echo $args['after_widget'];
	}

	public function form($instance)
	{

		$title     = isset($instance['title']) ? esc_attr($instance['title']) : '';
		$number    = isset($instance['number']) ? absint($instance['number']) : 5;
		$categoryname = isset($instance['categoryname']) ? esc_attr($instance['categoryname']) : '';
	?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>" />
		</p>

		<p><label for="<?php echo $this->get_field_id('number'); ?>"><?php _e('Number of posts to show:'); ?></label>
			<input class="tiny-text" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" type="number" step="1" min="1" value="<?php echo $number; ?>" size="3" />
		</p>

		<p><label for="<?php echo $this->get_field_id('categoryname'); ?>"><?php _e('Enter name Category:'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id('categoryname'); ?>" name="<?php echo $this->get_field_name('categoryname'); ?>" type="text" value="<?php echo $categoryname; ?>" />
		</p>
		<?php
	}
	public function update($new_instance, $old_instance)
	{
		$instance              = $old_instance;
		$instance['title']     = sanitize_text_field($new_instance['title']);
		$instance['number']    = (int) $new_instance['number'];
		$instance['categoryname'] = sanitize_text_field($new_instance['categoryname']);
		return $instance;
	}
}
/**
 * Extend Recent Posts Widget 
 *
 * Adds different formatting to the default WordPress Recent Posts Widget
 */

class My_Recent_Posts_Widget extends WP_Widget_Recent_Posts
{

	function widget($args, $instance)
	{

		extract($args);

		$title = apply_filters('widget_title', empty($instance['title']) ? __('Recent Posts') : $instance['title'], $instance, $this->id_base);

		if (empty($instance['number']) || !$number = absint($instance['number']))
			$number = 10;

		$r = new WP_Query(apply_filters('widget_posts_args', array('posts_per_page' => $number, 'no_found_rows' => true, 'post_status' => 'publish', 'ignore_sticky_posts' => true)));
		if ($r->have_posts()) :

			echo $before_widget;
		?>
			<div class="up-widget mb-6">

				<div class="fonts-sidebar-title position-relative">
					<?php
					if ($title) {
						echo  $title;
					}
					?>
				</div>
				<div class="popular-stories">
					<div class="popular-stories-list">
						<?php while ($r->have_posts()) : $r->the_post(); ?>
							<div class="clearfix mini-news-preview popular-story cb-editable"><a href="<?php the_permalink(); ?>">
									<div class="image-preview float-left">
										<img loading="lazy" src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" alt="<?php the_title(); ?>" />
									</div>
									<div class="fonts-popular-story-headline">
										<?php the_title(); ?>
									</div>
								</a></div>
						<?php endwhile; ?>
					</div>
				</div>


			</div>
<?php
			echo $after_widget;

			wp_reset_postdata();

		endif;
	}
}
function my_recent_widget_registration()
{
	unregister_widget('WP_Widget_Recent_Posts');
	register_widget('My_Recent_Posts_Widget');
}
add_action('widgets_init', 'my_recent_widget_registration');
