<!DOCTYPE html>
<html lang="en">

<head>
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="referrer" content="origin-when-cross-origin">
	<?php wp_head(); ?>
</head>

<body>
	<div id="site-box" class="site-box">
		<nav id="slidemenu" class="slidemenu-box">
			<?php
			wp_nav_menu(array(
				'theme_location' => 'primary',
				'container' => 'ul',
				'menu_class' => 'slidemenu',
			));
			?>
		</nav><button id="slidemenu-close-btn" class="slidemenu-close-btn" type="button" name="close-slidemenu"><i class="fas fa-times"></i></button>
		<div id="overlay"></div>
		<div class="container-fluid navbar__top pt-lg-5 pt-md-0 mr-0 pr-3">
			<div class="col-6 col-lg-4 pt-5 pl-xs-0 pl-lg-8 navbar__top__left">
				<?php get_search_form(); ?>
			</div>
			<div class="col-6 col-lg-4 text-center pt-3-lg navbar__top__brand"><a href="index.html" class="brand-logo"></a><button id="mainmenu-slidemenu-btn" type="button" name="button"></button>
			</div>
			<div class="col-4 pt-5 pr-8 navbar__top__right">
				<?php echo get_template_part("template-parts/social_seo", null); ?>
			</div>
		</div>
		<div class="affix-wrapper">
			<div data-spy="affix" data-offset-top="170" id="affix">
				<nav class="mainmenu">
					<div class="affix-brand"><a href="index.html" class="brand-logo brand-logo--sm ml-3"></a></div>
					<div class="mainmenu-links">
						<?php
						wp_nav_menu(array(
							'theme_location' => 'primary',
							'container' => 'ul',
							'menu_class' => 'mainmenu-list',
							'menu_id' => 'mainmenu-items'
						));
						?>
					</div>
				</nav>
			</div>
		</div>
		<section class="trending-bar--block d-none d-md-block">
			<div class="trending-bar">
				<?php
				$trending_categories = get_categories(
					array(
						'orderby'    => 'id',
						'order'      => 'DESC',
						'hide_empty' => '0',
						'meta_query' => array(
							array(
								'key'     => 'show_trending',     // Adjust to your needs!
								'value'   => true,   // Adjust to your needs!
								'compare' => '=',         // Default
							)
						)
					)
				);
				?>
				<ul>
					<li class="text-uppercase">
						Trending
					</li>
					<?php
					if (count($trending_categories) > 0) :
						foreach ($trending_categories as $trending) :
					?>
							<li><a class="px-4" href="<?php echo get_category_link($trending->term_id); ?>"><?php echo $trending->name; ?></a></li>
					<?php endforeach;
					endif; ?>
				</ul>
			</div>
		</section>