<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since Twenty Seventeen 1.0
 * @version 1.0
 */
get_header();
$args = array(
    "post_type" => "category",
    "orderby" => "count",
    "order"   => "DESC",
    "posts_per_page" => 12
);
$all_args = array(
    "post_type" => "category",
    "posts_per_page" => -1,
);
$categories = get_categories($args);
$all_categories = get_categories($all_args);
?>
<section id="content" class="content-block no-padding">
    <?php
    if (count($categories) > 0) :
    ?>
        <div class="home__main-hero-container pt-5 pb-10">
            <div class="container-fluid container-fluid--max pt-8 pb-3">
                <h1 class="page-title mb-5 cb-editable cb-editable--relative"></h1>
                <div class="row">
                    <?php
                    foreach ($categories as $cate) :
                    ?>
                        <div class=" col-md-3 col-lg-3 col-xl-2 card-container cb-editable">
                            <div class="cb-editable"><a href="<?php echo get_category_link($cate->term_id); ?>">
                                    <div class="up-card up-card--white up-card--sm-horizontal up-card--md-vertical-long up-card--lg-vertical-long up-card--xl-vertical-long">
                                        <img class="lazyload up-card__image up-card__image--small" src="<?php the_field("category_image", "term_" . $cate->term_id); ?>" alt="<?php echo $cate->slug; ?>" />
                                        <div class="up-card__text-container up-card--fit">
                                            <div class="up-card__text">
                                                <h3 class="up-card__title font-title font-md-4"><?php echo $cate->name; ?></h3>
                                                <div class="up-card__teaser"><?php echo $cate->description; ?></div>
                                            </div>
                                        </div>
                                        <div class="up-card__image-overlay up-card__image up-card__image--small"></div>
                                    </div>
                                    <div class="up-card up-card--white up-card--sm-horizontal-small d-block d-md-none border-top">
                                        <div class="up-card__text-container">
                                            <div class="up-card__text">
                                                <div class="up-card__teaser"><?php echo $cate->description; ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </a></div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
    <div class="slash-container-left-pink">
        <div class="container-fluid container-fluid--max px-3 pt-8 pb-10">
            <div class="up-menu">
                <div class="up-menu__button font-title font-weight-normal font-5 border-bottom">
                    <div class="up-menu__button-text">Filter Alphabetically</div>
                </div>
                <ul class="font-title font-5">
                    <li><a href="#A-D">
                            <div class="up-menu__title">A-D</div>
                        </a></li>
                    <li><a href="#E-G">
                            <div class="up-menu__title">E-G</div>
                        </a></li>
                    <li><a href="#H-K">
                            <div class="up-menu__title">H-K</div>
                        </a></li>
                    <li><a href="#L-O">
                            <div class="up-menu__title">L-O</div>
                        </a></li>
                    <li><a href="#P-S">
                            <div class="up-menu__title">P-S</div>
                        </a></li>
                    <li><a href="#T-W">
                            <div class="up-menu__title">T-W</div>
                        </a></li>
                    <li><a href="#X-Z">
                            <div class="up-menu__title">X-Z</div>
                        </a></li>
                </ul>
            </div>
            <div class="bg-white py-5 px-3">
                <div id="A-D">
                    <div class="pt-6 px-6">
                        <h2 class="font-title font-6 mb-5 cb-editable cb-editable--relative">
                            A-D
                        </h2>
                        <div class="row">
                            <?php
                            $arrStringAtoD = ["A", "B", "C", "D"];
                            foreach ($all_categories as $cate) :
                                if (in_array(strtoupper($cate->name[0]), $arrStringAtoD)) :
                            ?>
                                    <div class=" col-md-6 col-lg-3 col-xl-3 card-container cb-editable">
                                        <div class="cb-editable">
                                            <a href="<?php echo get_category_link($cate->term_id); ?>">
                                                <div class="up-card up-card--white up-card--sm-horizontal-compact up-card--md-horizontal-compact ">
                                                    <img class="lazyload up-card__image up-card__image--square" src="<?php the_field("category_image", "term_" . $cate->term_id); ?>" alt="<?php echo $cate->slug; ?>" />
                                                    <div class="up-card__text-container">
                                                        <div class="up-card__text">
                                                            <h3 class="up-card__title text-left font-title font-md-4 font-5 font-weight-normal font-weight-md-light">
                                                                <?php echo $cate->name;
                                                                ?> ( <?php echo $cate->count; ?> )</h3>
                                                            <div class="up-card__teaser"><?php echo $cate->description; ?></div>
                                                        </div>
                                                    </div>
                                                    <div class="up-card__image-overlay up-card__image up-card__image--square">
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                            <?php endif;
                            endforeach; ?>
                        </div>
                    </div>
                </div>
                <div id="E-G">
                    <div class="pt-6 px-6">
                        <h2 class="font-title font-6 mb-5 cb-editable cb-editable--relative">
                            E-G
                        </h2>
                        <div class="row">
                            <?php
                            $arrStringEtoG = ["E", "F", "G"];
                            foreach ($all_categories as $cate) :
                                if (in_array(strtoupper($cate->name[0]), $arrStringEtoG)) :
                            ?>
                                    <div class=" col-md-6 col-lg-3 col-xl-3 card-container cb-editable">
                                        <div class="cb-editable">
                                            <a href="<?php echo get_category_link($cate->term_id); ?>">
                                                <div class="up-card up-card--white up-card--sm-horizontal-compact up-card--md-horizontal-compact ">
                                                    <img class="lazyload up-card__image up-card__image--square" src="<?php the_field("category_image", "term_" . $cate->term_id); ?>" alt="<?php echo $cate->slug; ?>" />
                                                    <div class="up-card__text-container">
                                                        <div class="up-card__text">
                                                            <h3 class="up-card__title text-left font-title font-md-4 font-5 font-weight-normal font-weight-md-light">
                                                                <?php echo $cate->name;
                                                                ?> ( <?php echo $cate->count; ?> )</h3>
                                                            <div class="up-card__teaser"><?php echo $cate->description; ?></div>
                                                        </div>
                                                    </div>
                                                    <div class="up-card__image-overlay up-card__image up-card__image--square">
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                            <?php endif;
                            endforeach; ?>
                        </div>
                    </div>
                </div>
                <div id="H-K">
                    <div class="pt-6 px-6">
                        <h2 class="font-title font-6 mb-5 cb-editable cb-editable--relative">
                            H-K
                        </h2>
                        <div class="row">
                            <?php
                            $arrStringHtoK = ["H", "I", "J", "K"];
                            foreach ($all_categories as $cate) :
                                if (in_array(strtoupper($cate->name[0]), $arrStringHtoK)) :
                            ?>
                                    <div class=" col-md-6 col-lg-3 col-xl-3 card-container cb-editable">
                                        <div class="cb-editable">
                                            <a href="<?php echo get_category_link($cate->term_id); ?>">
                                                <div class="up-card up-card--white up-card--sm-horizontal-compact up-card--md-horizontal-compact ">
                                                    <img class="lazyload up-card__image up-card__image--square" src="<?php the_field("category_image", "term_" . $cate->term_id); ?>" alt="<?php echo $cate->slug; ?>" />
                                                    <div class="up-card__text-container">
                                                        <div class="up-card__text">
                                                            <h3 class="up-card__title text-left font-title font-md-4 font-5 font-weight-normal font-weight-md-light">
                                                                <?php echo $cate->name;
                                                                ?> ( <?php echo $cate->count; ?> )</h3>
                                                            <div class="up-card__teaser"><?php echo $cate->description; ?></div>
                                                        </div>
                                                    </div>
                                                    <div class="up-card__image-overlay up-card__image up-card__image--square">
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                            <?php endif;
                            endforeach; ?>
                        </div>
                    </div>
                </div>
                <div id="L-O">
                    <div class="pt-6 px-6">
                        <h2 class="font-title font-6 mb-5 cb-editable cb-editable--relative">
                            L-O
                        </h2>
                        <div class="row">
                            <?php
                            $arrStringLtoO = ["L", "M", "N", "O"];
                            foreach ($all_categories as $cate) :
                                if (in_array(strtoupper($cate->name[0]), $arrStringLtoO)) :
                            ?>
                                    <div class=" col-md-6 col-lg-3 col-xl-3 card-container cb-editable">
                                        <div class="cb-editable">
                                            <a href="<?php echo get_category_link($cate->term_id); ?>">
                                                <div class="up-card up-card--white up-card--sm-horizontal-compact up-card--md-horizontal-compact ">
                                                    <img class="lazyload up-card__image up-card__image--square" src="<?php the_field("category_image", "term_" . $cate->term_id); ?>" alt="<?php echo $cate->slug; ?>" />
                                                    <div class="up-card__text-container">
                                                        <div class="up-card__text">
                                                            <h3 class="up-card__title text-left font-title font-md-4 font-5 font-weight-normal font-weight-md-light">
                                                                <?php echo $cate->name;
                                                                ?> ( <?php echo $cate->count; ?> )</h3>
                                                            <div class="up-card__teaser"><?php echo $cate->description; ?></div>
                                                        </div>
                                                    </div>
                                                    <div class="up-card__image-overlay up-card__image up-card__image--square">
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                            <?php endif;
                            endforeach; ?>
                        </div>
                    </div>
                </div>
                <div id="P-S">
                    <div class="pt-6 px-6">
                        <h2 class="font-title font-6 mb-5 cb-editable cb-editable--relative">
                            P-S
                        </h2>
                        <div class="row">
                            <?php
                            $arrStringPtoS = ["P", "Q", "R", "S"];
                            foreach ($all_categories as $cate) :
                                if (in_array(strtoupper($cate->name[0]), $arrStringPtoS)) :
                            ?>
                                    <div class=" col-md-6 col-lg-3 col-xl-3 card-container cb-editable">
                                        <div class="cb-editable">
                                            <a href="<?php echo get_category_link($cate->term_id); ?>">
                                                <div class="up-card up-card--white up-card--sm-horizontal-compact up-card--md-horizontal-compact ">
                                                    <img class="lazyload up-card__image up-card__image--square" src="<?php the_field("category_image", "term_" . $cate->term_id); ?>" alt="<?php echo $cate->slug; ?>" />
                                                    <div class="up-card__text-container">
                                                        <div class="up-card__text">
                                                            <h3 class="up-card__title text-left font-title font-md-4 font-5 font-weight-normal font-weight-md-light">
                                                                <?php echo $cate->name;
                                                                ?> ( <?php echo $cate->count; ?> )</h3>
                                                            <div class="up-card__teaser"><?php echo $cate->description; ?></div>
                                                        </div>
                                                    </div>
                                                    <div class="up-card__image-overlay up-card__image up-card__image--square">
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                            <?php endif;
                            endforeach; ?>
                        </div>
                    </div>
                </div>
                <div id="T-W">
                    <div class="pt-6 px-6">
                        <h2 class="font-title font-6 mb-5 cb-editable cb-editable--relative">
                            T-W
                        </h2>
                        <div class="row">
                            <?php
                            $arrStringTtoW = ["T", "U", "V", "W"];
                            foreach ($all_categories as $cate) :
                                if (in_array(strtoupper($cate->name[0]), $arrStringTtoW)) :
                            ?>
                                    <div class=" col-md-6 col-lg-3 col-xl-3 card-container cb-editable">
                                        <div class="cb-editable">
                                            <a href="<?php echo get_category_link($cate->term_id); ?>">
                                                <div class="up-card up-card--white up-card--sm-horizontal-compact up-card--md-horizontal-compact ">
                                                    <img class="lazyload up-card__image up-card__image--square" src="<?php the_field("category_image", "term_" . $cate->term_id); ?>" alt="<?php echo $cate->slug; ?>" />
                                                    <div class="up-card__text-container">
                                                        <div class="up-card__text">
                                                            <h3 class="up-card__title text-left font-title font-md-4 font-5 font-weight-normal font-weight-md-light">
                                                                <?php echo $cate->name;
                                                                ?> ( <?php echo $cate->count; ?> )</h3>
                                                            <div class="up-card__teaser"><?php echo $cate->description; ?></div>
                                                        </div>
                                                    </div>
                                                    <div class="up-card__image-overlay up-card__image up-card__image--square">
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                            <?php endif;
                            endforeach; ?>
                        </div>
                    </div>
                </div>
                <div id="X-Z">
                    <div class="pt-6 px-6">
                        <h2 class="font-title font-6 mb-5 cb-editable cb-editable--relative">
                            X-Z
                        </h2>
                        <div class="row">
                            <?php
                            $arrStringXtoZ = ["X", "Y", "Z"];
                            foreach ($all_categories as $cate) :
                                if (in_array(strtoupper($cate->name[0]), $arrStringXtoZ)) :
                            ?>
                                    <div class=" col-md-6 col-lg-3 col-xl-3 card-container cb-editable">
                                        <div class="cb-editable">
                                            <a href="<?php echo get_category_link($cate->term_id); ?>">
                                                <div class="up-card up-card--white up-card--sm-horizontal-compact up-card--md-horizontal-compact ">
                                                    <img class="lazyload up-card__image up-card__image--square" src="<?php the_field("category_image", "term_" . $cate->term_id); ?>" alt="<?php echo $cate->slug; ?>" />
                                                    <div class="up-card__text-container">
                                                        <div class="up-card__text">
                                                            <h3 class="up-card__title text-left font-title font-md-4 font-5 font-weight-normal font-weight-md-light">
                                                                <?php echo $cate->name;
                                                                ?> ( <?php echo $cate->count; ?> )</h3>
                                                            <div class="up-card__teaser"><?php echo $cate->description; ?></div>
                                                        </div>
                                                    </div>
                                                    <div class="up-card__image-overlay up-card__image up-card__image--square">
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                            <?php endif;
                            endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
get_footer();
?>