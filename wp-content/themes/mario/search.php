<?php
get_header();
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$search_term = get_search_query();
$args = array(
    'posts_per_page' => 20,
    'post_type' => 'post',
    'post_status' => array('publish'),
    "s" => $search_term,
    'numberposts' => -1,
    'paged' => $paged
);
$query_search = new WP_Query($args);
$total_posts = $query_search->found_posts;
?>
<section id="content" class="content-block ">
    <div id="postSearch" class="container-fluid container-fluid--max basic-page">
        <section class="row">
            <header class="col-lg-8 offset-lg-2 basic-page-head basic-page-search mt-6">
                <h1 id="searchResults" data-search-results="<?php echo $total_posts; ?>"><?php echo $total_posts; ?> Search Results</h1>
                <form class="input-group py-5" action="<?php echo home_url('/'); ?>" route="" method="get">
                    <input type="search" name="s" value="<?php echo isset($search_term) ? $search_term : ""; ?>" placeholder="Search…" class="form-control">
                    <div class="input-group-append">
                        <button class="btn btn-search" type="submit" value="">
                            <svg class="svg-inline--fa fa-search fa-w-16" aria-hidden="true" data-prefix="fas" data-icon="search" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                                <path fill="currentColor" d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"></path>
                            </svg>
                            <!-- <i class="fas fa-search"></i> --><span>Search</span>
                        </button>
                    </div>
                </form>
            </header>
        </section>
        <section class="row">
            <ul class="card-block card-block-search col-lg-8 offset-lg-2">
                <?php
                while ($query_search->have_posts()) : $query_search->the_post();
                ?>
                    <li class="card--search">
                        <div class="card-meta"><a href="<?php echo get_permalink(); ?>" class="card-type"><?php echo get_the_category($post->ID)[0]->cat_name; ?></a><span class="card-date"><?php echo date("M d, Y", strtotime($post->post_date)); ?></span></div>
                        <a href="<?php echo get_permalink(); ?>">
                            <div class="card-copy">
                                <h2><?php the_title(); ?></h2>
                            </div>
                        </a>
                    </li>
                <?php endwhile; ?>
            </ul>
        </section>
        <section class="row">
            <div class="col-lg-8 offset-lg-2 pb-6">
                <?php if (function_exists("pagination")) {
                    pagination($query_search->max_num_pages);
                } ?>
            </div>
        </section>
    </div>
</section>
<?php get_footer(); ?>