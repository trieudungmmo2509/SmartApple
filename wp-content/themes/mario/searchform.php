<?php

/**
 * Template Name: Search Page
 */
?>
<dl class="d-flex align-items-center user-search-container">
    <dd class="search-container">
        <div class="search-box">
            <form class="search-form" action="<?php echo home_url('/'); ?>" method="get">
                <input type="search" class="search-form__input" name="s" value="<?php echo get_search_query(); ?>" placeholder="SEARCH" autocomplete="off" required />
                <button type="submit" class="btn search-form__btn"><i class="fas fa-search"></i></button>
            </form>
        </div>
    </dd>
</dl>