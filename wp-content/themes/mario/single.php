<?php
get_header();
global $post;
$category_post = get_the_category($post->ID);
$arrCategory = [];
foreach ($category_post as $cate) {
	$arrCategory[] = $cate->term_id;
}
$post_relate = query_post_current($post->ID, $arrCategory);
?>
<section id="content" class="content-block container">
	<div class="row article-section fonts-article-section">
		<div class="col-15">
			<?php foreach ($category_post as $cate) : ?>
				<a href="<?php echo get_category_link($cate->term_id); ?>">
					<?php echo $cate->cat_name . ","; ?>
				</a>
			<?php endforeach; ?>
		</div>
	</div>
	<hr class="article-top-divider" />
	<div>
		<link href="atrial-fibrillation-explained-4220.html">
		<div class="row article-header ">
			<div class="col-15">
				<h1 class="article-title fonts-article-headline"><?php echo $post->post_title; ?></h1>
				<div class="article-sub-title fonts-article-sub"><?php echo get_post_meta($post->ID, "description", true); ?></div>
			</div>
		</div>
		<div class="row article-image text-left">
			<div id="article-content" class="col-xl-11 col-15">
				<div><img src="<?php echo get_the_post_thumbnail_url($post->ID); ?>" alt="Atrial fibrillation explained" />
					<meta content="660">
					<meta content="1180">
				</div>
				<div class="py-3 text-gray-600 text-md-center m-auto position-relative">
					<?php echo get_post(get_post_thumbnail_id())->post_content; ?>
				</div>
				<div class="row d-lg-none mini-article-info fonts-article-meta ">
					<div class="col-md-15 col-15">
						<div class="article-author-meta">
							<div class="article-date">
								<?php echo date("M d, Y", strtotime($post->post_date)); ?>
							</div>
							<meta content="2020-02-18T14:42:17+00:00">
							<div class="article-author-info "><span class="article-author">
									By <a rel="author" href=""><span>Admin</span></a></span><span class="article-author-twitter"><span>
										@admin
									</span></span></div>
						</div>
						<div class="article-media-icons"><span class="fa-layers fa-fw fa-lg"><span title="Facebook"><i class="fas fa-circle"></i><i class="fab fa-facebook-f fa-inverse" data-fa-transform="shrink-6"></i></span></span><span class="fa-layers fa-fw fa-lg"><span data-url="http://twitter.com/intent/tweet?text=Atrial fibrillation explained: Why Apple wants to look after your heart https%3A%2F%2Fwww.wareable.com%2Ffitbit%2Fatrial-fibrillation-explained-4220" title="Twitter"><i class="fas fa-circle"></i><i class="fab fa-twitter fa-inverse" data-fa-transform="shrink-6"></i></span></span><span class="fa-layers fa-fw fa-lg"><span data-url="http://www.reddit.com/submit?url=https%3A%2F%2Fwww.wareable.com%2Ffitbit%2Fatrial-fibrillation-explained-4220" title="Reddit"><i class="fas fa-circle"></i><i class="fab fa-reddit-alien fa-inverse" data-fa-transform="shrink-6"></i></span></span><span class="fa-layers fa-fw fa-lg"><span data-url="https://getpocket.com/edit?url=https%3A%2F%2Fwww.wareable.com%2Ffitbit%2Fatrial-fibrillation-explained-4220&amp;title=Atrial%20fibrillation%20explained%3A%20Why%20Apple%20wants%20to%20look%20after%20your%20heart" title="Pocket"><i class="fas fa-circle"></i><i class="fab fa-get-pocket fa-inverse" data-fa-transform="shrink-6"></i></span></span></div>
						<hr class="article-body-divider" />
					</div>
				</div>
				<div class="row article-main">
					<div class="col-xl-4 col-lg-4 d-none d-lg-block fonts-article-meta">
						<div class="article-day">
							<?php echo date("D", strtotime($post->post_date)); ?>
						</div>
						<div class="article-date">
							<?php echo date("M d, Y", strtotime($post->post_date)); ?>
						</div>
						<meta content="	<?php echo date("M d, Y", strtotime($post->post_date)); ?>">
						<div class="article-author">
							By <a rel="author" href=""><span>Admin</span></a></div>
						<div class="article-author-twitter"><span>
								@admin
							</span></div>
						<div class="article-media-icons"><span class="fa-layers fa-fw fa-lg"><span title="Facebook"><i class="fas fa-circle"></i><i class="fab fa-facebook-f fa-inverse" data-fa-transform="shrink-6"></i></span></span><span class="fa-layers fa-fw fa-lg"><span data-url="http://twitter.com/intent/tweet?text=Atrial fibrillation explained: Why Apple wants to look after your heart https%3A%2F%2Fwww.wareable.com%2Ffitbit%2Fatrial-fibrillation-explained-4220" title="Twitter"><i class="fas fa-circle"></i><i class="fab fa-twitter fa-inverse" data-fa-transform="shrink-6"></i></span></span><span class="fa-layers fa-fw fa-lg"><span data-url="http://www.reddit.com/submit?url=https%3A%2F%2Fwww.wareable.com%2Ffitbit%2Fatrial-fibrillation-explained-4220" title="Reddit"><i class="fas fa-circle"></i><i class="fab fa-reddit-alien fa-inverse" data-fa-transform="shrink-6"></i></span></span><span class="fa-layers fa-fw fa-lg"><span data-url="https://getpocket.com/edit?url=https%3A%2F%2Fwww.wareable.com%2Ffitbit%2Fatrial-fibrillation-explained-4220&amp;title=Atrial%20fibrillation%20explained%3A%20Why%20Apple%20wants%20to%20look%20after%20your%20heart" title="Pocket"><i class="fas fa-circle"></i><i class="fab fa-get-pocket fa-inverse" data-fa-transform="shrink-6"></i></span></span></div>
					</div>
					<div class="col-xl-11 col-lg-11 col-xs-15 col-15">
						<div id="article-body" class="fonts-article-body rich-text">
							<?php
							echo $post->post_content;
							?>
						</div>
						<div class="tagged"><span class="tagged-header fonts-tagged-header">TAGGED</span>
							<?php
							$tags = get_the_tags($post->ID);
							if (!empty($tags)) :
								foreach ($tags as $tag) :
							?>
									<a href="javascript:void(0);" class="tag fonts-tagged"><?php echo $tag->name; ?></a>
							<?php endforeach;
							endif; ?>
						</div>

						<hr class="article-body-divider" />
						<?php
						if ($post_relate->have_posts()) :
						?>
							<div class="recent-stories-alt pb-6">
								<div class="fonts-sidebar-title position-relative">

									Related stories

								</div>
								<?php
								while ($post_relate->have_posts()) : $post_relate->the_post();
								?>
									<div class="recent-story-alt cb-editable"><span class="section fonts-recent-stories-alt-tags"><?php echo get_the_category($post->ID)[0]->cat_name; ?></span><a href="<?php echo get_permalink(); ?>" class="title fonts-recent-stories-alt-title"><?php the_title(); ?></a></div>
								<?php
								endwhile;
								?>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-11 offset-lg-4"></div>
				</div>
			</div>
			<?php
			wpse_get_template_part("template-parts/sidebar-right", null, array("postId" => $post->ID));
			?>
		</div>
	</div>
</section>
<?php
get_footer();
?>