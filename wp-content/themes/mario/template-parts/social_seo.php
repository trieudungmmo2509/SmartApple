<?php $social_data = get_option('wpseo_social', true); ?>
<dl class="d-flex social-links">
    <dd class="social-link-item"><a class="btn-twitter" href="" title="Twitter" rel="noopener"><i class="fab fa-twitter"></i></a></dd>
    <dd class="social-link-item"><a class="btn-facebook" href="<?php echo $social_data["facebook_site"]; ?>" title="Facebook" target="_blank" rel="noopener"><i class="fab fa-facebook-square"></i></a>
    </dd>
    <dd class="social-link-item"><a class="btn-instagram" href="<?php echo $social_data["instagram_url"]; ?>" title="instagram" target="_blank" rel="noopener"><i class="fab fa-instagram"></i></a></dd>
    <dd class="social-link-item"><a class="btn-rss" href=""><i class="fas fa-rss"></i></a></dd>
</dl>