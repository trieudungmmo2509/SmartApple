<?php
$posts_home_top = query_posts_with_by_category($category->term_id, $number_post);
if ($posts_home_top->have_posts()) :
?>
    <div class="home__main-hero-container pt-3 pt-xl-5">
        <div class="container-fluid container-fluid--max pt-xl-8">
            <div class="border-bottom bdr-color--gray-lighter pb-7">
                <h1 class="container-fluid container-fluid--max home__section-title mb-5 d-none">
                    <?php echo $title; ?>
                </h1>
                <div class="row">
                    <?php
                    $i = 0;
                    while ($posts_home_top->have_posts()) : $posts_home_top->the_post();
                        $i++;
                    ?><?php
                        if ($i == 1) :
                        ?>
                    <div class="col-lg-9 col-md-6 card-container">
                        <div class="cb-editable"><a href="<?php echo get_permalink(); ?>">
                                <div class="up-card up-card--primary up-card--sm-vertical-large up-card--md-vertical up-card--lg-horizontal-large up-card--xl-horizontal-large">
                                    <img loading="lazy" class="lazyload up-card__image up-card__image--large" sizes="
        (min-width: 1260px) 625px,
        (min-width: 992px) 625px,
        (min-width: 768px) 625px,
        625px" src="<?php echo get_the_post_thumbnail_url(get_the_ID()) ?>" alt="<?php the_title(); ?>" />
                                    <div class="up-card__text-container">
                                        <div class="up-card__text">
                                            <h3 class="up-card__title"><?php the_title(); ?></h3>
                                            <div class="up-card__teaser"><?php echo get_post_meta(get_the_ID(), "description", true); ?></div>
                                        </div>
                                    </div>
                                    <div class="up-card__image-overlay up-card__image up-card__image--large">
                                    </div>
                                </div>
                            </a></div>
                    </div>
                <?php elseif ($i == 2) : ?>
                    <div class="col-lg-3 col-md-6 card-container">
                        <div class="cb-editable"><a href="<?php echo get_permalink(); ?>">
                                <div class="up-card up-card--white up-card--sm-horizontal up-card--md-vertical up-card--lg-vertical up-card--xl-vertical">
                                    <img loading="lazy" class="up-card__image up-card__image--small" sizes="
        (min-width: 1260px) 150px,
        (min-width: 992px) 150px,
        (min-width: 768px) 625px,
        150px" src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" alt="<?php the_title(); ?>" />
                                    <div class="up-card__text-container">
                                        <div class="up-card__text">
                                            <h3 class="up-card__title"><?php the_title(); ?></h3>
                                            <div class="up-card__teaser"><?php echo get_post_meta(get_the_ID(), "description", true); ?></div>
                                        </div>
                                    </div>
                                    <div class="up-card__image-overlay up-card__image up-card__image--small">
                                    </div>
                                </div>
                            </a></div>
                    </div>
                <?php else : ?>
                    <div class="col-lg-6 col-md-12 card-container">
                        <div class="cb-editable"><a href="<?php echo get_permalink(); ?>">
                                <div class="up-card up-card--white up-card--sm-horizontal up-card--md-horizontal up-card--lg-horizontal up-card--xl-horizontal">
                                    <div class="up-card__text-container">
                                        <div class="up-card__text">
                                            <h3 class="up-card__title"><?php the_title(); ?></h3>
                                            <div class="up-card__teaser"><?php echo get_post_meta(get_the_ID(), "description", true); ?></div>
                                        </div>
                                    </div><img loading="lazy" class="up-card__image up-card__image--small" sizes="
        (min-width: 1260px) 150px,
        (min-width: 992px) 150px,
        (min-width: 768px) 625px,
        150px" src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" alt="<?php the_title(); ?>" />
                                    <div class="up-card__image-overlay up-card__image up-card__image--small">
                                    </div>
                                </div>
                            </a></div>
                    </div>
            <?php
                        endif;
                    endwhile;
            ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>