<?php
$posts_latest_stories = query_posts_with_by_category($category->term_id, $number_post);
if ($posts_latest_stories->have_posts()) :
?>
    <div class="container-fluid container-fluid--max pt-7 pb-5">
        <div class="border-bottom bdr-color--gray-lighter pb-7">
            <div class="text-center mb-5 cb-editable cb-editable--relative">
                <h2 class="d-inline home__section-title"></h2>
            </div>
            <div class="text-center mb-5 cb-editable cb-editable--relative">
                <h2 class="d-inline home__section-title">
                    <?php echo $title; ?>
                </h2>
            </div>
            <div class="row">
                <?php
                $i = 0;
                while ($posts_latest_stories->have_posts()) : $posts_latest_stories->the_post();
                    $i++;
                ?>
                    <?php
                    if ($i < 9) :
                    ?>
                        <div class=" col-md-12 col-lg-3 card-container cb-editable">
                            <div class="cb-editable"><a href="<?php echo get_permalink(); ?>">
                                    <div class="up-card up-card--white up-card--sm-horizontal-small up-card--md-horizontal up-card--lg-vertical up-card--xl-vertical">
                                        <img  loading="lazy" class="up-card__image up-card__image--small" sizes="
        (min-width: 1260px) 150px,
        (min-width: 992px) 150px,
        (min-width: 768px) 625px,
        150px" src="<?php echo get_the_post_thumbnail_url(get_the_ID()) ?>" alt="​<?php the_title(); ?>" />
                                        <div class="up-card__text-container">
                                            <div class="up-card__text">
                                                <h3 class="up-card__title">​<?php the_title(); ?></h3>
                                                <div class="up-card__teaser"><?php echo get_post_meta(get_the_ID(), "description", true); ?></div>
                                            </div>
                                        </div>
                                        <div class="up-card__image-overlay up-card__image up-card__image--small"></div>
                                    </div>
                                </a></div>
                        </div>
                    <?php else : ?>
                        <div class=" col-md-12 col-lg-6 card-container cb-editable">
                            <div class="cb-editable"><a href="<?php echo get_permalink(); ?>">
                                    <div class="up-card up-card--white up-card--sm-horizontal up-card--md-horizontal up-card--lg-horizontal up-card--xl-horizontal">
                                        <img loading="lazy" class="up-card__image up-card__image--small" sizes="
        (min-width: 1260px) 150px,
        (min-width: 992px) 150px,
        (min-width: 768px) 625px,
        150px" src="<?php echo get_the_post_thumbnail_url(get_the_ID()); ?>" alt="<?php the_title(); ?>" />
                                        <div class="up-card__text-container">
                                            <div class="up-card__text">
                                                <h3 class="up-card__title"><?php the_title(); ?></h3>
                                                <div class="up-card__teaser"><?php echo get_post_meta(get_the_ID(), "description", true); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="up-card__image-overlay up-card__image up-card__image--small"></div>
                                    </div>
                                </a></div>
                        </div>
                <?php
                    endif;
                endwhile;
                ?>
            </div>
        </div>
    </div>
<?php endif; ?>